import CartParser from './CartParser';
import path from 'path';

let parser, parse, validate, parseLine, calcTotal, createError;

jest.mock('uuid', () => {
	return {
		v4: jest.fn(() => 1)
	}
})

const cartItems = {
	"items": [
		{
			"id": 1,
			"name": "Mollis consequat",
			"price": 9,
			"quantity": 2
		},
		{
			"id": 1,
			"name": "Tvoluptatem",
			"price": 10.32,
			"quantity": 1
		},
		{
			"id": 1,
			"name": "Scelerisque lacinia",
			"price": 18.9,
			"quantity": 1
		},
		{
			"id": 1,
			"name": "Consectetur adipiscing",
			"price": 28.72,
			"quantity": 10
		},
		{
			"id": 1,
			"name": "Condimentum aliquet",
			"price": 13.9,
			"quantity": 1
		}
	],
	"total": 348.32
}

const filePath = './samples/cart.csv';

beforeEach(() => {
	parser = new CartParser();

	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal;
	createError = parser.createError;
});

describe('CartParser - unit tests', () => {
	describe('parser.validate', () => {
		it('should return an empty array when data is valid', () => {
			const content = 'Product name,Price,Quantity\nMollis consequat,9.00,2'
			const expectedOutput = [];

			expect(validate(content)).toEqual(expectedOutput);
		});

		it('should return an error because of unexpected naming of header', () => {
			const content = 'Product name, Test\nMollis consequat,9.00,2';
			const expectedOutput = [
				{
					type: "header",
					row: 0,
					column: 1,
					message: 'Expected header to be named "Price" but received Test.'
				},
				{
					type: "header",
					row: 0,
					column: 2,
					message:
						'Expected header to be named "Quantity" but received undefined.'
				}
			];

			expect(validate(content)).toEqual(expectedOutput);
		});

		it('should return an error because of unexpected row length', () => {
			const content = 'Product name, Price, Quantity\nMollis consequat, 9.00';
			const expectedOutput = [
				{
					type: "row",
					row: 1,
					column: -1,
					message: "Expected row to have 3 cells but received 2."
				}
			];

			expect(validate(content)).toEqual(expectedOutput);
		});

		it('should return an error because of cell', () => {
			const content = 'Product name, Price, Quantity\n, 2 ,9.00';
			const expectedOutput = [
				{
					type: "cell",
					row: 1,
					column: 0,
					message: 'Expected cell to be a nonempty string but received "".'
				}
			];

			expect(validate(content)).toEqual(expectedOutput);
		});

		it('should return an error because of null in a cell', () => {
			const content = `Product name, Price, Quantity\nMollis consequat, 9.00, ${null}`;
			const expectedOutput = [
				{
					type: "cell",
					row: 1,
					column: 2,
					message: 'Expected cell to be a positive number but received "null".'
				}
			];

			expect(validate(content)).toEqual(expectedOutput);
		});

		it('should return an error because of string in a number cell', () => {
			const content = 'Product name, Price, Quantity\nMollis consequat, 9.00, test';
			const expectedOutput = [
				{
					type: "cell",
					row: 1,
					column: 2,
					message: 'Expected cell to be a positive number but received "test".'
				}
			];

			expect(validate(content)).toEqual(expectedOutput);
		});
	});

	describe('parser.parse', () => {
		it('should return an error with incorrect filename extension', () => {
			expect(() => {
				return parse(filePath + 'a');
			}).toThrow();
		});
	});

	describe('parser.parseLine', () => {
		it('should convert a line to one item object', () => {
			const content = 'Mollis consequat,9.00,2';
			const expectedOutput = {
				"id": 1,
				"name": "Mollis consequat",
				"price": 9,
				"quantity": 2
			};

			expect(parseLine(content)).toEqual(expectedOutput);
		});
	});

	describe('parser.calcTotal', () => {
		it('should return the total price', () => {
			expect(calcTotal(cartItems.items)).toBeCloseTo(348.32, 2);
		});

		it('should return an error if data is null', () => {
			expect(() => calcTotal(null)).toThrow();
		});
	});

	describe('parser.createError', () => {
		it('should return correct error object', () => {
			expect(createError('cell', 0, 1, 'test')).toEqual(
				{
					type: 'cell',
					row: 0,
					column: 1,
					message: 'test'
				});
		});
	});
});

describe('CartParser - integration test', () => {
	it('should parse file correctly', () => {
		const actualResult = parser.parse(path.resolve(__dirname, '../samples/cart.csv'));

		expect(actualResult).toBeTruthy();
		expect(actualResult).toEqual(cartItems);
		expect(actualResult).toHaveProperty("total", 348.32);
		expect(actualResult).toHaveProperty("items");
		expect(actualResult.items.length).toBe(5);
	})
});
